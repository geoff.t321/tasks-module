import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  private tasksRoute = 'http://localhost:3000/tasks';
  public task = {}

  constructor(private http: HttpClient,  private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.http.get(`${this.tasksRoute}/${params['id']}`).subscribe(data => {
        this.task = data;
      })
    });
  }
}
