import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksComponent } from './tasks/tasks.component';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { TaskComponent } from './task/task.component';
import { CreateTaskComponent } from './create-task/create-task.component';

const routes: Routes = [
    {
        path: 'tasks',
        component: TasksComponent
    },
    {
      path: 'create-task',
      component: CreateTaskComponent
    },
    {
        path: 'edit-task/:id',
        component: EditTaskComponent
    },
    {
        path: 'task/:id',
        component: TaskComponent
    },
    {
        path: '',
        component: TasksComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
