import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.scss']
})
export class EditTaskComponent implements OnInit {
  private tasksRoute = 'http://localhost:3000/tasks';
  public editTask = {}

  constructor(private http: HttpClient,  private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.http.get(`${this.tasksRoute}/${params['id']}`).subscribe(data => {
        this.editTask = data;
      })
    });
  }

  submitTask(){
    this.http.put(`${this.tasksRoute}/${this.editTask['id']}`, this.editTask).subscribe(data => {
      this.editTask = {};
      console.log('Submited task successfully');
      this.router.navigate(['/tasks']);
    })
  }
}
