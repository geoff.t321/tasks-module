import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  private tasksRoute = 'http://localhost:3000/tasks';
  public tasks;

  constructor(private http: HttpClient) { }

  getTasks() {
    this.http.get(this.tasksRoute).subscribe(tasks => {
        console.log(tasks, 'tasks list');
        this.tasks = tasks;
    });
  }

  ngOnInit() {
    this.getTasks();  
  }

  deleteTask(taskId){
    this.http.delete(`${this.tasksRoute}/${taskId}`).subscribe(tasks => {
      console.log(tasks, 'tasks deleted');
      this.getTasks();
  });
  }
}
