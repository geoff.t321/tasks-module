import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {
  private tasksRoute = 'http://localhost:3000/tasks';
  public newTask = {}

  constructor(private http: HttpClient,  private router: Router) { }

  ngOnInit() {
    this.newTask = {
      title: null,
      description: null, 
      points: 0
    }
  }

  submitTask(){
    this.http.post(this.tasksRoute, this.newTask).subscribe(data => {
      this.newTask = {};
      console.log('Submited task successfully');
      this.router.navigate(['/tasks']);
    })
  }

}
